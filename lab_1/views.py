from django.shortcuts import render
from datetime import datetime, date

# Enter your name here
mhs_name = 'Razaqa Dhafin Haffiyan'
univ = 'Universitas Indonesia'
hobby = 'Make Something'
desc = 'I am a big dreamer'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999,4,5)
npm = 1706039484
img_url = 'https://bem.cs.ui.ac.id/staf/assets/2018/img/photo/1706039484.jpg'
bckgrd_url = 'https://images.adsttc.com/media/images/5017/aeef/28ba/0d44/3100/0c6d/slideshow/stringio.jpg?1414573411'

tmn1_mhs_name = 'Ardanto FInkan Septa'
tmn1_univ = 'Universitas Indonesia'
tmn1_hobby = 'Swimming'
tmn1_desc = ''
tmn1_curr_year = int(datetime.now().strftime("%Y"))
tmn1_birth_date = date(1999,11,14)
tmn1_npm = 1706039736
tmn1_img_url = 'http://ardanto-fs.herokuapp.com/myself.JPG'

tmn2_mhs_name = 'Dzaky Noor Hasyim'
tmn2_univ = 'Universitas Indonesia'
tmn2_hobby = 'cycling, airsoft skirmish-ing, and looking for more cool hobbies'
tmn2_desc = 'a normal college student here, kinda decent lol and I have some knowledge in war machinery, especially in WW2 era'
tmn2_curr_year = int(datetime.now().strftime("%Y"))
tmn2_birth_date = date(1999,1,7)
tmn2_npm = 1706021606
tmn2_img_url = 'https://scontent.fcgk4-2.fna.fbcdn.net/v/t1.0-9/37383008_2588934234465791_7718569208920932352_n.jpg?_nc_cat=0&oh=63c2fd45162b3f15ca730ff4d1a154de&oe=5C37ACC0'
tmn2_bckgrd_url = 'https://images.adsttc.com/media/images/5017/aeef/28ba/0d44/3100/0c6d/slideshow/stringio.jpg?1414573411'

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year),
                'birth':birth_date, 'univ':univ, 'npm': npm, 'desc':desc,
				'hobby':hobby, 'img_url':img_url, 'bckgrd_url':bckgrd_url,
				'tmn1_name': tmn1_mhs_name, 'tmn1_age': calculate_age(tmn1_birth_date.year),
                'tmn1_birth':tmn1_birth_date, 'tmn1_univ':tmn1_univ, 'tmn1_npm': tmn1_npm, 'tmn1_desc':tmn1_desc,
				'tmn1_hobby':tmn1_hobby, 'tmn1_img_url':tmn1_img_url,
				'tmn2_name': tmn2_mhs_name, 'tmn2_age': calculate_age(tmn2_birth_date.year),
                'tmn2_birth':tmn2_birth_date, 'tmn2_univ':tmn2_univ, 'tmn2_npm': tmn2_npm, 'tmn2_desc':tmn2_desc,
				'tmn2_hobby':tmn2_hobby, 'tmn2_img_url':tmn2_img_url,
				}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
